//
//  GameScene.h
//  First
//
//  Created by Alexandru Andronache on 9/12/13.
//
//

#ifndef __First__GameScene__
#define __First__GameScene__

#include "cocos2d.h"

class AVBoard;
class AVGameHud;

using namespace cocos2d;

class GameScene : public CCLayer
{
public:
    // test commit
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();
    
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::CCScene* scene();
    
    // a selector callback
    void menuCloseCallback(CCObject* pSender);
    
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(GameScene);

//    bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
//    void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
//    void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
//    void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

private:
    AVBoard*                  mBoard;
    AVGameHud*                mGameHud;
};

#endif /* defined(__First__GameScene__) */
