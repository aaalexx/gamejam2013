//
//  AVBug.cpp
//  First
//
//  Created by UrAnus on 9/12/13.
//
//

#include "AVBug.h"
#include "CCStdC.h"

USING_NS_CC;

// ******************************************************************************************************************
AVBug::AVBug()
        : CCNodeRGBA()
        , mBubbleSprite(NULL)
        , mState(eStateIdle)
        , mDuration(0)
{

}

// ******************************************************************************************************************
AVBug::~AVBug()
{
    
}

// ******************************************************************************************************************
bool AVBug::init()
{
    mBubbleSprite = cocos2d::CCSprite::create("Star.png");

//    CCAnimation* anim = CCAnimation::create();
//    anim->init();
//    anim->addSpriteFrameWithFileName("1_star.png");
//    anim->addSpriteFrameWithFileName("2_star.png");
//    anim->addSpriteFrameWithFileName("3_star.png");
//    anim->setLoops(-1);
//    anim->setDelayPerUnit(0.1f);
//
//    addChild(CCSprite::create("1_star.png"));
////    addChild(CCSprite::create("2_star.png"));
////    addChild(CCSprite::create("3_star.png"));
//
//    runAction(CCRepeatForever::create(CCAnimate::create(anim)));

  // addChild(mBubbleSprite);
  //  mBubbleSprite->setScale(0.3f);
    return true;
}

// ******************************************************************************************************************

void AVBug::circlePoint(cocos2d::CCPoint point, float time)
{
    if (mState != eStateHover)
    {
        mInitPos = point;
        mState = eStateHover;
        mCircDuration = time;
        stopAllActions();
        doCircleMove();
    }
}

// ******************************************************************************************************************

void AVBug::doCircleMove()
{
    CCArray sequence;

    CCPoint cPos = mInitPos;

    ccBezierConfig config;
    config.controlPoint_1 = ccp(cPos.x + CCRANDOM_MINUS1_1() * 10,
                                cPos.y + CCRANDOM_MINUS1_1() * 10);

    config.controlPoint_2 = ccp(cPos.x + CCRANDOM_MINUS1_1() * 10,
                                cPos.y + CCRANDOM_MINUS1_1() * 10);

    config.endPosition = ccp(MIN(MAX(cPos.x + CCRANDOM_MINUS1_1() * 10, cPos.x - 30), cPos.x + 30),
                             MIN(MAX(cPos.y + CCRANDOM_MINUS1_1() * 10, cPos.y - 30), cPos.y + 30));

    CCFiniteTimeAction* actionMove = CCBezierTo::create(mCircDuration,
                                                        config);
    CCFiniteTimeAction* actionMoveDone =
    CCCallFuncN::create(this,
                        callfuncN_selector(AVBug::onCircleComplete));
    sequence.addObject(actionMove);
    sequence.addObject(CCDelayTime::create(mCircDuration * CCRANDOM_0_1()));
    sequence.addObject(actionMoveDone);

    mBubbleSprite->runAction(CCSequence::create(&sequence));
}

// ******************************************************************************************************************

void AVBug::onCircleComplete()
{
    doCircleMove();
}

// ******************************************************************************************************************

void AVBug::moveToPoint(cocos2d::CCPoint point, float time)
{
    // if (mState == eStateHover)
    {
        mInitPos = point;
        mDuration = time;
        mState = eStateMove;
        // stopAllActions();
        doMoveTo();
    }
}

// ******************************************************************************************************************

void AVBug::doMoveTo()
{
    CCArray sequence;

    CCPoint cPos = mInitPos;

    ccBezierConfig config;
    config.controlPoint_1 = ccp(cPos.x + CCRANDOM_MINUS1_1() * 3,
                                cPos.y + CCRANDOM_MINUS1_1() * 3);

    config.controlPoint_2 = ccp(cPos.x + CCRANDOM_MINUS1_1() * 3,
                                cPos.y + CCRANDOM_MINUS1_1() * 3);
    config.endPosition = mInitPos;

//    CCFiniteTimeAction* actionMove = CCBezierTo::create(mDuration,
//                                                        config);

    CCFiniteTimeAction* actionMove2 = CCMoveTo::create(mDuration, config.endPosition);
    CCFiniteTimeAction* actionMoveDone =
    CCCallFuncN::create(this,
                        callfuncN_selector(AVBug::onMoveComplete));
    sequence.addObject(actionMove2);
    sequence.addObject(actionMoveDone);

    mBubbleSprite->runAction(CCSequence::create(&sequence));
}

// ******************************************************************************************************************

void AVBug::onMoveComplete()
{
    circlePoint(mInitPos, mCircDuration);
}

// ******************************************************************************************************************

