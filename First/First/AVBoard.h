//
//  Board.h
//  First
//
//  Created by Alexandru Andronache on 9/12/13.
//
//

#ifndef __First__Board__
#define __First__Board__

#include <iostream>

#include <cocos2d.h>
#include "AVTile.h"
#include "AVBug.h"

class AVGameHud;

class AVBoard : public cocos2d::CCNode,
                public cocos2d::CCTouchDelegate
{
public:
    AVBoard();
    ~AVBoard();
    
    void generateBoard();

    bool init();
    void draw();

    bool ccTouchBegan(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent);
    void ccTouchMoved(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent);
    void ccTouchEnded(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent);
    void ccTouchCancelled(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent);
    
    void initGameHud(AVGameHud *gameHud);

private:

    enum eBoardState
    {
        eStateIdle = 0,
        eStateCecking,
        eStateAnimating,
        eStateFinished
    };

    AVTile***                   mBoard;
    cocos2d::CCSprite***        mGrid;

    cocos2d::CCSpriteBatchNode* mBatchGrid;
    cocos2d::CCSpriteBatchNode* mBatchStars;


    cocos2d::CCArray            mChainTiles;
    cocos2d::CCArray            mRemoveTiles;

    eBoardState                 mState;
    AVTile::eTileType           mStartTileType;
    AVGameHud*                  mGameHud;
    cocos2d::CCLabelTTF*        mEndGameLabel;
    bool                        mGameEnd;

    cocos2d::CCArray            mBugs;

    AVTile*                     getTileUnderPoint(const cocos2d::CCPoint& pPoint);

    void                        consumeChain(void* node = NULL);

    cocos2d::CCArray            getTileNeighbours(AVTile* tile);

    void                        onTileScaleComplete(void* tile);


    void                        showEndGame();

    void                        releaseBugs(AVTile* tile);
    void                        configTile(AVTile* tile, cocos2d::CCPoint position);
    
    bool                        areNeighboorTiles(AVTile* tile1, AVTile* tile2);

};

#endif /* defined(__First__Board__) */
