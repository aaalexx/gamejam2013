//
//  AVBug.h
//  First
//
//  Created by UrAnus on 9/12/13.
//
//

#ifndef __First__AVBug__
#define __First__AVBug__

#include <iostream>

#include "cocos2d.h"


class AVBug : public cocos2d::CCNodeRGBA
{
public:

    enum eBugState
    {
        eStateIdle,
        eStateAppear,
        eStateHover,
        eStateMove,
        eStateDissapear
    };

    AVBug();
    ~AVBug();

    bool init();

    void circlePoint(cocos2d::CCPoint point, float time);
    void moveToPoint(cocos2d::CCPoint point, float time);

    cocos2d::CCSprite*      getSprite()
    {
        return mBubbleSprite;
    }

private:
    cocos2d::CCSprite*      mBubbleSprite;
    eBugState               mState;

    cocos2d::CCPoint        mInitPos;
    float                   mDuration;
    float                   mCircDuration;

    void        doCircleMove();
    void        onCircleComplete();
    void        doMoveTo();
    void        onMoveComplete();
};
#endif /* defined(__First__AVBug__) */
