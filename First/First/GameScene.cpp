//
//  GameScene.cpp
//  First
//
//  Created by Alexandru Andronache on 9/12/13.
//
//

#include "GameScene.h"
#include "AVBoard.h"
#include "AVGameHud.h"
#include "HelloWorldScene.h"

using namespace cocos2d;

// ******************************************************************************************************************
CCScene* GameScene::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    GameScene *layer = GameScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}


// ******************************************************************************************************************
// on "init" you need to initialize your instance
bool GameScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }
    
    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.
    
    // add a "close" icon to exit the progress. it's an autorelease object
    CCMenuItemImage *pCloseItem = CCMenuItemImage::create(
                                                          "CloseNormal.png",
                                                          "CloseSelected.png",
                                                          this,
                                                          menu_selector(GameScene::menuCloseCallback) );
    pCloseItem->setPosition( ccp(CCDirector::sharedDirector()->getWinSize().width - 20, 20) );
    
    // create menu, it's an autorelease object
    CCMenu* pMenu = CCMenu::create(pCloseItem, NULL);
    pMenu->setPosition( CCPointZero );
    this->addChild(pMenu, 1);
    
    /////////////////////////////
    // 3. add your codes below...
    
    // add a label shows "Hello World"
    // create and initialize a label
//    CCLabelTTF* pLabel = CCLabelTTF::create("Game scene", "Thonburi", 34);
//    
//    // ask director the window size
    CCSize size = CCDirector::sharedDirector()->getWinSize();
//
//    // position the label on the center of the screen
//    pLabel->setPosition( ccp(size.width / 2, size.height - 20) );
//    
//    // add the label as a child to this layer
//    this->addChild(pLabel, 1);
    
    // add "HelloWorld" splash screen"
    CCSprite* pSprite = CCSprite::create("leaf.jpg");
    
    // position the sprite on the center of the screen
    pSprite->setPosition( ccp(size.width/2, size.height/2) );
    
    // add the sprite as a child to this layer
    this->addChild(pSprite, 0);

    mBoard = new AVBoard();
    mBoard->init();
    mGameHud = new AVGameHud();
    mGameHud->init();
    mBoard->initGameHud(mGameHud);

    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(mBoard, 0, true);

    this->addChild(mBoard);
    this->addChild(mGameHud);

    mBoard->setPosition(250, 0);

    return true;
}

// ******************************************************************************************************************
void GameScene::menuCloseCallback(CCObject* pSender)
{
    CCScene *s = HelloWorld::scene();
    
    CCDirector::sharedDirector()->setDepthTest(true);
    CCTransitionScene *transition = CCTransitionPageTurn::create(3.0f, s, false);
    CCDirector::sharedDirector()->replaceScene(transition);
}

// ******************************************************************************************************************
//bool GameScene::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
//{
//    return mBoard->ccTouchBegan(pTouch, pEvent);
//}
//
//// ******************************************************************************************************************
//void GameScene::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
//{
//    mBoard->ccTouchMoved(pTouch, pEvent);
//}
//
//// ******************************************************************************************************************
//void GameScene::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
//{
//    mBoard->ccTouchEnded(pTouch, pEvent);
//}
//
//// ******************************************************************************************************************
//void GameScene::ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent)
//{
//    mBoard->ccTouchCancelled(pTouch, pEvent);
//}



// ******************************************************************************************************************
