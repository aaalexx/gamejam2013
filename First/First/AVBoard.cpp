//
//  Board.cpp
//  First
//
//  Created by Alexandru Andronache on 9/12/13.
//
//

#include "AVBoard.h"
#include "AVGameHud.h"
#include "HelloWorldScene.h"

USING_NS_CC;

const int SIZE = 10;

#define SPRITE_MOVE_DOWN        0.15f
#define SPRITE_REMOVE           0.06f
#define SPRITE_SCALE_TIME       0.1f
#define SPRITE_SCALE_FACTOR     1.5f

#define SPRITE_NEIGHBOUR_MOVE_TIME  0.09f
#define SPRITE_NEIGHBOUR_MOVE_FACT  0.2f

#define BUG_CIRC_TIME           2.f
#define BUG_MOVE_TIME           0.5f

#define TILE_COUNT              10

#define TILE_WIDTH              60
#define TILE_OFFSET             10
#define BOARD_OFFSET            50

#define SCORE_FOR_CHAIN         100
#define SCORE_FOR_BURST         1000

#define END_GAME_TEXT_SIZE      130

// ******************************************************************************************************************
AVBoard::AVBoard()
        : cocos2d::CCNode()
        , mBoard(NULL)
        , mState(eStateIdle)
        , mStartTileType(AVTile::eTileNone)
        , mEndGameLabel(NULL)
        , mGameEnd(false)
{

    mBatchGrid = CCSpriteBatchNode::create("grid.png");
    addChild(mBatchGrid);

    mBoard = new AVTile**[SIZE];
    mGrid = new CCSprite**[SIZE];
    for (int i = 0; i < SIZE; ++i)
    {
        mBoard[i] = new AVTile*[SIZE];
        mGrid[i] = new CCSprite*[SIZE];
        for (int j = 0; j < SIZE; j++)
        {
            mBoard[i][j] = new AVTile();
            mBoard[i][j]->setZOrder(50);

            mGrid[i][j] = CCSprite::create("grid.png");
        }
    }

    mBatchStars = CCSpriteBatchNode::create("Star.png");
    addChild(mBatchStars);
    mBatchStars->setZOrder(100);
}

// ******************************************************************************************************************
AVBoard::~AVBoard()
{
//    for (int i = 0; i < SIZE; ++i)
//    {
//        delete[] board[i];
//    }
//    delete[] board;

}

// ******************************************************************************************************************
bool AVBoard::init()
{
    srand(time(NULL));
    for (int i = 0; i < SIZE; ++i)
    {
        for (int j = 0; j < SIZE; j++)
        {

            CCPoint pp = ccp(i * (TILE_WIDTH + TILE_OFFSET) + BOARD_OFFSET,
                             j * (TILE_WIDTH + TILE_OFFSET) + BOARD_OFFSET);

            mGrid[i][j]->setPosition(pp);
            mBatchGrid->addChild(mGrid[i][j]);

            mBoard[i][j]->init();
            addChild(mBoard[i][j]);
            configTile(mBoard[i][j], pp);
        }
    }
    
    mEndGameLabel = cocos2d::CCLabelTTF::create("", "Thonburi", END_GAME_TEXT_SIZE);
    mEndGameLabel->setPosition((CCDirector::sharedDirector()->getWinSize())/ 2 - ccp(250, 50));
    mEndGameLabel->setColor(ccWHITE);
    mEndGameLabel->setVisible(false);
    mEndGameLabel->setZOrder(150);
    
    this->addChild(mEndGameLabel, 150);

    return true;
}

// ******************************************************************************************************************
void AVBoard::draw()
{
    
}

// ******************************************************************************************************************
void AVBoard::initGameHud(AVGameHud *gameHud)
{
    mGameHud = gameHud;
}

// ******************************************************************************************************************
void AVBoard::configTile(AVTile* tile, CCPoint position)
{
    int counter = (int)(CCRANDOM_0_1() * 2);
    int i = 0;
    while (i++ < counter)
    {
        AVBug* bug = new AVBug();
        bug->init();
        mBatchStars->addChild(bug->getSprite());
  //      bug->setZOrder(100);
        bug->setPosition(position);
        bug->circlePoint(position, BUG_CIRC_TIME);
        mBugs.addObject(bug);
        tile->addBug(bug);
    }

    tile->setCounter(counter);
    tile->setType((AVTile::eTileType)(1 + CCRANDOM_0_1() * (AVTile::eTileCount - 1)));
    tile->setPosition(position);
    tile->setState(AVTile::eStateIdle);
}

// ******************************************************************************************************************
void AVBoard::generateBoard()
{
    for (int i = 0; i < SIZE; i++)
    {
        for (int j = 0; j < SIZE; j++)
        {
            if (mBoard[i][j]->getState() >= AVTile::eStateDead)
            {
                int k = j;
                for (; k < SIZE; k++)
                {
                    if (mBoard[i][k]->getState() < AVTile::eStateDead)
                    {
                        break;
                    }
                }

                AVTile* temp;
                if (k == SIZE)
                {
                    srandom(time(NULL));
                    configTile(mBoard[i][j], ccp(i * (TILE_WIDTH + TILE_OFFSET) + BOARD_OFFSET,
                                                 (j + 1) * (TILE_WIDTH + TILE_OFFSET) + BOARD_OFFSET));
                }
                else
                {
                    temp = mBoard[i][j];
                    mBoard[i][j] = mBoard[i][k];
                    mBoard[i][k] = temp;
                }
                    

                CCPoint endPoint = ccp(i * (TILE_WIDTH + TILE_OFFSET) + BOARD_OFFSET,
                                       j * (TILE_WIDTH + TILE_OFFSET) + BOARD_OFFSET);
                // Create the actions
                CCFiniteTimeAction* actionMove =
                CCMoveTo::create(SPRITE_MOVE_DOWN, endPoint);
                mBoard[i][j]->runAction(CCSequence::create(actionMove,
                                                          NULL, NULL) );

                for (int k = 0; k < mBoard[i][j]->getBugs().count(); k++)
                {
                    ((AVBug*)mBoard[i][j]->getBugs().objectAtIndex(k))->moveToPoint(endPoint, BUG_MOVE_TIME);
                }

            }
        }
    }
}

// ******************************************************************************************************************
bool AVBoard::ccTouchBegan(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent)
{
    // CCLOG("touch x : %f y : %f", pTouch->getLocationInView().x, pTouch->getLocationInView().y);
    // CCLOG("board x : %f y : %f", getPosition().x, getPosition().y);
    
    if (mGameEnd)
    {
        CCScene *s = HelloWorld::scene();
        
        CCDirector::sharedDirector()->setDepthTest(true);
        CCTransitionScene *transition = CCTransitionPageTurn::create(3.0f, s, false);
        CCDirector::sharedDirector()->replaceScene(transition);
    }

    if (mState == eStateIdle)
    {
        mChainTiles.removeAllObjects();
        cocos2d::CCPoint touch = pTouch->getLocation() - getPosition() + ccp(TILE_WIDTH / 2, TILE_WIDTH / 2);
        AVTile* startTile = getTileUnderPoint(touch);
        if (startTile)
        {
            CCLOG("startTile x : %f y : %f", startTile->getPosition().x, startTile->getPosition().y);
            mStartTileType = startTile->getType();
            mState = eStateCecking;
            mChainTiles.addObject(startTile);
            startTile->setState(AVTile::eStateOn);
            return true;
        }
    }
    return false;
}

// ******************************************************************************************************************
void AVBoard::ccTouchMoved(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent)
{
    if (mState == eStateCecking)
    {
        cocos2d::CCPoint touch = pTouch->getLocation() - getPosition() + ccp(TILE_WIDTH / 2, TILE_WIDTH / 2);
        AVTile* tile = getTileUnderPoint(touch);
        if (tile &&
            !mChainTiles.containsObject(tile) &&
            tile->getType() == mStartTileType &&
            areNeighboorTiles((AVTile*)mChainTiles.lastObject(), tile))
        {
            tile->setState(AVTile::eStateOn);
            mChainTiles.addObject(tile);
        }
    }
    else
    {
    }
}

// ******************************************************************************************************************
void AVBoard::ccTouchEnded(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent)
{
    if (mState == eStateCecking)
    {
        int i = 0;
        for (i = 0; i < mChainTiles.count() - 1; ++i)
        {
            if (!areNeighboorTiles((AVTile*)mChainTiles.objectAtIndex(i),
                                   (AVTile*)mChainTiles.objectAtIndex(i + 1)))
            {
                break;
            }
        }
        
        while (mChainTiles.count() > i + 1)
        {
            ((AVTile*)mChainTiles.lastObject())->setState(AVTile::eStateOff);
            mChainTiles.removeLastObject();
        }
        
        if (mChainTiles.count() > 2)
        {
            mGameHud->incMoves(-1);
            
            
            mState = eStateAnimating;
            
            consumeChain();
            
            if (mGameHud->getGameEndState() == AVGameHud::EGameEndLost)
            {
                showEndGame();
            }
        }
        else
        {
            for (int i = 0; i < mChainTiles.count(); i++)
            {
                ((AVTile*)mChainTiles.objectAtIndex(i))->setState(AVTile::eStateOff);
            }
            mState = eStateIdle;
        }
    }
    else
    {
    }
}

// ******************************************************************************************************************
void AVBoard::ccTouchCancelled(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent)
{
    ccTouchEnded(pTouch, pEvent);
}


// ******************************************************************************************************************
AVTile* AVBoard::getTileUnderPoint(const cocos2d::CCPoint& pPoint)
{

    // @TODO optimize later

    for (int i = 0; i < SIZE; i++)
    {
        for (int j = 0; j < SIZE; j++)
        {
            cocos2d::CCRect rect(mBoard[i][j]->getPosition().x, mBoard[i][j]->getPosition().y,
                                 TILE_WIDTH, TILE_WIDTH);
            if (rect.containsPoint(pPoint))
            {
                return mBoard[i][j];
            }
        }
    }
    return NULL;
}


// ******************************************************************************************************************
void AVBoard::consumeChain(void* node)
{
//    int tileCount = 0;
//    if (node)
//    {
//        AVTile* tile = (AVTile*) node;
//        tile->setState(AVTile::eStateDead);
//        tileCount = tile->getCounter();
//    }

    if (mChainTiles.count() > 1)
    {
        for (int i = 0; i < mChainTiles.count(); i++)
        {
            AVTile* tile = (AVTile*)mChainTiles.objectAtIndex(i);
            tile->setState(AVTile::eStateOff);

            CCArray sequence;
            if (i == mChainTiles.count() - 1)
            {
                mChainTiles.removeObjectAtIndex(i);
                mRemoveTiles.addObject(tile);

                CCFiniteTimeAction* actionMove = CCMoveTo::create(SPRITE_REMOVE,
                                                                  tile->getPosition());
                CCFiniteTimeAction* actionMoveDone =
                CCCallFuncND::create(this,
                                     callfuncND_selector(AVBoard::consumeChain), (void*) tile);
                sequence.addObject(actionMove);
                sequence.addObject(actionMoveDone);
      //          tile->setCounter(tile->getCounter() + tileCount);
            }
            else
            {
                AVTile* tile2 = (AVTile*)mChainTiles.objectAtIndex(i + 1);
                // Create the actions
                CCFiniteTimeAction* actionMove = CCMoveTo::create(SPRITE_REMOVE, tile2->getPosition());
                sequence.addObject(actionMove);
            }

            tile->runAction(CCSequence::create(&sequence));
        }
    }
    else
    {
        mGameHud->incScore(mRemoveTiles.count() * SCORE_FOR_CHAIN);

        mRemoveTiles.addObject(mChainTiles.objectAtIndex(0));

        AVTile* lastTile = (AVTile*)mRemoveTiles.objectAtIndex(0);

        int count = 0;
        while (mRemoveTiles.count() - 1 > 0)
        {
            AVTile* tile = (AVTile*)mRemoveTiles.lastObject();
            mRemoveTiles.removeLastObject();
            tile->setState(AVTile::eStateDead);
            count += tile->getCounter();

            while (tile->getBugs().count())
            {
                AVBug* bug = (AVBug*)tile->getBugs().lastObject();
                lastTile->addBug(bug);
                tile->getBugs().removeLastObject();
                bug->moveToPoint(lastTile->getPosition(), BUG_MOVE_TIME);
            }
        }

        // lastTile = (AVTile*)mRemoveTiles.objectAtIndex(0);
        mRemoveTiles.removeLastObject();
        count += lastTile->getCounter();
        lastTile->setCounter(count);


        CCArray sequence;
        // Create the actions
        CCFiniteTimeAction* actionScaleUp = CCScaleBy::create(SPRITE_SCALE_TIME, SPRITE_SCALE_FACTOR);
        sequence.addObject(actionScaleUp);

        if (count >= TILE_COUNT)
        {
            releaseBugs(lastTile);
            CCFiniteTimeAction* actionFadeOut = CCFadeOut::create(SPRITE_SCALE_TIME);
            sequence.addObject(actionFadeOut);
            CCDirector::sharedDirector()->setAlphaBlending(true);
        }
        else
        {
            CCFiniteTimeAction* actionScaleDown = CCScaleBy::create(SPRITE_SCALE_TIME, 1.0f / SPRITE_SCALE_FACTOR);
            sequence.addObject(actionScaleDown);
        }

        CCFiniteTimeAction* actionScaleComplete =
                            CCCallFuncND::create(this,
                                                 callfuncND_selector(AVBoard::onTileScaleComplete), lastTile);
        sequence.addObject(actionScaleComplete);

        lastTile->runAction(CCSequence::create(&sequence));

        CCArray neighbours = getTileNeighbours(lastTile);
        for (int i = 0; i < neighbours.count(); i++)
        {
            AVTile* neighbourTile = (AVTile*)neighbours.objectAtIndex(i);

            CCPoint origin = neighbourTile->getPosition();
            CCPoint offset = (origin - lastTile->getPosition()) * SPRITE_NEIGHBOUR_MOVE_FACT;
            CCFiniteTimeAction* actionMoveAway = CCMoveBy::create(SPRITE_NEIGHBOUR_MOVE_TIME, offset);
            CCFiniteTimeAction* actionMoveBack = CCMoveTo::create(SPRITE_NEIGHBOUR_MOVE_TIME, origin);

            CCArray neighbourSeqence;
            neighbourSeqence.addObject(actionMoveAway);
            neighbourSeqence.addObject(actionMoveBack);
            neighbourTile->runAction(CCSequence::create(&neighbourSeqence));
        }
    }
}

// ******************************************************************************************************************
void AVBoard::releaseBugs(AVTile* tile)
{
    int count = tile->getCounter();
    if (count >= TILE_COUNT)
    {
        mGameHud->incScore(SCORE_FOR_BURST);
        mGameHud->addObjective(tile->getType());
        if (mGameHud->getGameEndState() == AVGameHud::EGameEndVictory)
        {
            showEndGame();
        }
        
        CCArray sameTypeTiles;
        for (int i = 0; i < SIZE; i++)
        {
            for (int j = 0; j < SIZE; j++)
            {
                if (mBoard[i][j]->getType() == mStartTileType &&
                    mBoard[i][j] != tile)
                {
                    sameTypeTiles.addObject(mBoard[i][j]);
                }
            }
        }

        while (count > 0 && sameTypeTiles.count())
        {
            AVTile* tile2 = (AVTile*)sameTypeTiles.objectAtIndex((int)(CCRANDOM_0_1() * sameTypeTiles.count()));
            tile2->intCounter(1);

            AVBug* bug = (AVBug*)tile->getBugs().lastObject();
            tile->getBugs().removeLastObject();
            tile2->addBug(bug);
            bug->moveToPoint(tile2->getPosition(), BUG_MOVE_TIME);

            count--;
        }

        //tile->setState(AVTile::eStateBurst);
    }
}

// ******************************************************************************************************************
void AVBoard::onTileScaleComplete(void* pTile)
{
    AVTile* tile = (AVTile*)pTile;
    if (tile->getCounter() >= TILE_COUNT)
    {
        // CCDirector::sharedDirector()->setAlphaBlending(false);
        tile->setState(AVTile::eStateBurst);
    }

    generateBoard();
    mState = eStateIdle;
}

// ******************************************************************************************************************
CCArray AVBoard::getTileNeighbours(AVTile *tile)
{
    CCArray ret;
    for (int i = 0; i < SIZE; i++)
    {
        for (int j = 0; j < SIZE; j++)
        {
            if (mBoard[i][j] == tile)
            {
                if (i > 0)
                {
                    ret.addObject(mBoard[i - 1][j]);
                    if (j > 0)
                    {
                        ret.addObject(mBoard[i - 1][j - 1]);
                    }
                    if (j < SIZE -1)
                    {
                        ret.addObject(mBoard[i - 1][j + 1]);
                    }
                }
                if (i < SIZE - 1)
                {
                    ret.addObject(mBoard[i + 1][j]);
                    if (j > 0)
                    {
                        ret.addObject(mBoard[i + 1][j - 1]);
                    }
                    if (j < SIZE -1)
                    {
                        ret.addObject(mBoard[i + 1][j + 1]);
                    }
                }
                if (j < SIZE - 1)
                {
                    ret.addObject(mBoard[i][j + 1]);
                }
                if (j > 0)
                {
                    ret.addObject(mBoard[i][j - 1]);
                }
            }
        }
    }

    return ret;
}

// ******************************************************************************************************************

void AVBoard::showEndGame()
{
    mGameEnd  = true;
    mEndGameLabel->setVisible(true);
    if (mGameHud->getGameEndState() == AVGameHud::EGameEndLost)
    {
        mEndGameLabel->setString("You Lost");
    }
    if (mGameHud->getGameEndState() == AVGameHud::EGameEndVictory)
    {
        mEndGameLabel->setString("GG! You won");
    }
}

// ******************************************************************************************************************

bool AVBoard::areNeighboorTiles(AVTile* tile1, AVTile* tile2)
{
    int i1, j1, i2, j2;
    for (int i = 0; i < SIZE; ++i)
    {
        for (int j = 0; j < SIZE; ++j)
        {
            if (tile1 == mBoard[i][j])
            {
                i1 = i;
                j1 = j;
            }
            if (tile2 == mBoard[i][j])
            {
                i2 = i;
                j2 = j;
            }
        }
    }
    
    if (abs(i2 - i1) <= 1 && abs(j1 - j2) <= 1)
    {
        return true;
    }
    return false;
}



