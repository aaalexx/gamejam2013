//
//  Hud.cpp
//  First
//
//  Created by Alexandru Andronache on 9/12/13.
//
//

#include "AVGameHud.h"

#define MOVES_NUMBER 30
#define TEXT_SCORE_SIZE 40

#define X_OFFSET 80
#define Y_OFFSET 60
#define Y_DIFF_OFFSET 100

AVGameHud::AVGameHud()
        : mScoreLabel(NULL)
        , mMovesLabel(NULL)
        , mObjectivesLabel(NULL)
        , mMoves(MOVES_NUMBER)
        , mScore(0)
{
}

AVGameHud::~AVGameHud()
{
    
}

bool AVGameHud::init()
{
    mObjectives[0].number = 3;
    mObjectives[0].type = AVTile::eTileBlue;
    mObjectives[1].number = 2;
    mObjectives[1].type = AVTile::eTileGreen;
    
    mScoreLabel = cocos2d::CCLabelTTF::create("", "Thonburi", TEXT_SCORE_SIZE);
    mScoreLabel->setPosition(ccp(X_OFFSET, Y_OFFSET));
    
    mMovesLabel = cocos2d::CCLabelTTF::create("", "Thonburi", TEXT_SCORE_SIZE);
    mMovesLabel->setPosition(ccp(X_OFFSET, Y_OFFSET + Y_DIFF_OFFSET));
    
    mObjectivesLabel = cocos2d::CCLabelTTF::create("", "Thonburi", TEXT_SCORE_SIZE);
    mObjectivesLabel->setPosition(ccp(X_OFFSET + 20, Y_OFFSET + 5 * Y_DIFF_OFFSET));

    this->addChild(mScoreLabel);
    this->addChild(mMovesLabel);
    this->addChild(mObjectivesLabel);
    
    updateLabels();
    
    return true;
}

void AVGameHud::draw()
{
    
}

void AVGameHud::incScore(int score)
{
    mScore += score;
    updateLabels();
}

int AVGameHud::getScore()
{
    return mScore;
}

void AVGameHud::updateLabels()
{
    char textScore[25], textMoves[25], textObjectives[100];
    sprintf(textScore, "Score \n %d", mScore);
    mScoreLabel->setString(textScore);
    sprintf(textMoves, "Moves \n %d", mMoves);
    mMovesLabel->setString(textMoves);
    sprintf(textObjectives, "Objectives\n Type: BLUE\nNumber:%d\nType:GREEN\nNumber:%d", mObjectives[0].number, mObjectives[1].number);
    mObjectivesLabel->setString(textObjectives);
}

void AVGameHud::incMoves(int moves)
{
    mMoves += moves;
    updateLabels();
}

void AVGameHud::addObjective(AVTile::eTileType type)
{
    for (int i = 0; i < NUM_OBJECTIVES; ++i)
    {
        if (mObjectives[i].type == type)
        {
            if (mObjectives[i].number > 0)
            {
                mObjectives[i].number--;
            }
        }
    }
    updateLabels();
}

AVGameHud::EGameEnd AVGameHud::getGameEndState()
{
    if (mMoves <= 0)
    {
        return AVGameHud::EGameEndLost;
    }
    
    bool objectivesCompleted = true;
    for (int i = 0; i < NUM_OBJECTIVES; ++i)
    {
        if (mObjectives[i].number > 0)
        {
            objectivesCompleted = false;
        }
    }
    
    if (objectivesCompleted)
    {
        return AVGameHud::EGameEndVictory;
    }
    
    return AVGameHud::EGameEndNone;
}
