//
//  Tile.h
//  First
//
//  Created by Alexandru Andronache on 9/12/13.
//
//

#ifndef __First__Tile__
#define __First__Tile__

#include <iostream>

#include "cocos2d.h"
#include "AVBug.h"

class AVTile : public cocos2d::CCNodeRGBA
{
public:
    enum eTileState
    {
        eStateNone = 0,
        eStateIdle,
        eStateOn,
        eStateOff,
        eStateBurst,
        eStateDead
    };

    enum eTileType
    {
        eTileNone = 0,
        eTileRed,
        eTileBlue,
        eTileYellow,
        eTileGreen,
      //  eTilePurple,
        eTileCount
    };

    AVTile();
    ~AVTile();

    bool init();
    void draw();

    void setType(eTileType type);
    eTileType getType()
    {
        return mType;
    }

    cocos2d::CCSprite*      getSprite()
    {
        return mBubbleSprite;
    }

    void setState(eTileState state);
    eTileState getState()
    {
        return mState;
    }

    void intCounter(int val)
    {
        setCounter(mCounter + val);
        updateLabel();
    }
    void setCounter(int counter)
    {
        mCounter = counter;
        setScale(1.f);
        updateLabel();
    }
    
    int getCounter()
    {
        return mCounter;
    }

    void addBug(AVBug* bug);
    
    cocos2d::CCArray& getBugs()
    {
        return mBugs;
    }

    void setPosition(const cocos2d::CCPoint& point);

    void hideLabel();

private:
    eTileType               mType;
    eTileState              mState;
    cocos2d::CCSprite*      mBubbleSprite;
    int                     mCounter;
    cocos2d::CCLabelTTF*    mLabel;
    cocos2d::CCArray        mBugs;

    void configType();
    void setLabel();
    void updateLabel();
    void updateBugs();
};

#endif /* defined(__First__Tile__) */
