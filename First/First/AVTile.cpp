//
//  Tile.cpp
//  First
//
//  Created by Alexandru Andronache on 9/12/13.
//
//

#include "AVTile.h"

USING_NS_CC;

#define TEXT_SIZE 18
#define TEXT_OFFSET -10


// ******************************************************************************************************************
AVTile::AVTile()
        : CCNodeRGBA()
        , mType(eTileNone)
        , mState(eStateNone)
        , mBubbleSprite(NULL)
        , mCounter(0)
        , mLabel(NULL)
{
    
}


// ******************************************************************************************************************
AVTile::~AVTile()
{
    
}

// ******************************************************************************************************************
bool AVTile::init()
{
    setLabel();
    setCounter(0);
    setType(eTileNone);
    return true;
}

// ******************************************************************************************************************

void AVTile::setType(AVTile::eTileType type)
{
    if (mType != type)
    {
        setScale(1.f);
        mType = type;
        configType();
    }
}


// ******************************************************************************************************************
void AVTile::draw()
{
}

// ******************************************************************************************************************
void AVTile::configType()
{
    if (mBubbleSprite)
    {
        mBubbleSprite->removeFromParent();
//        mBubbleSprite->release();
//        delete mBubbleSprite;
        mBubbleSprite = NULL;
    }
    switch (mType) {
        case eTileBlue:
            mBubbleSprite = cocos2d::CCSprite::create("tile_blue_1.png");
            break;
//        case eTilePurple:
//            mBubbleSprite = cocos2d::CCSprite::create("tile_purple_1.png");
//            break;
        case eTileGreen:
            mBubbleSprite = cocos2d::CCSprite::create("tile_green_1.png");
            break;
        case eTileRed:
            mBubbleSprite = cocos2d::CCSprite::create("tile_red_1.png");
            break;
        case eTileYellow:
            mBubbleSprite = cocos2d::CCSprite::create("tile_yellow_1.png");
            break;
        case eTileNone:
            hideLabel();
        default:
            return;
            break;
    }

    // add the sprite as a child to this layer
    // mBubbleSprite->setAnchorPoint(cocos2d::CCPointZero);
    this->addChild(mBubbleSprite, 0);
}

// ******************************************************************************************************************

void AVTile::setState(AVTile::eTileState state)
{
    if (state != mState)
    {
        mState = state;
        switch (mState)
        {
            case eStateIdle:
            {
//                CCLiquid* liq = CCLiquid::create(10, CCSizeMake(10, 10), 10, 2);
//                this->runAction(CCSequence::create(liq, NULL, NULL));
            }
                break;
            case eStateOn:
            {
                setScale(1.f);

                CCFiniteTimeAction* actionScale = CCScaleTo::create(0.2f, 1.3f);
                //CCFiniteTimeAction* actionTint = CCTintTo::create(0.2f, -128, -128, -128);

                mBubbleSprite->runAction(CCSequence::create(actionScale, NULL, NULL));
                mState = eStateIdle;
            }
                break;
            case eStateOff:
            {
                CCFiniteTimeAction* actionScale = CCScaleTo::create(0.2f, 1.f);
          //      CCFiniteTimeAction* actionTint = CCTintTo::create(0.2f, 128, 128, 128);

                mBubbleSprite->runAction(CCSequence::create(actionScale, NULL, NULL));
                mState = eStateIdle;

            }
                break;
            case eStateBurst:
            {
                stopAllActions();
                setState(eStateDead);
            }
                
                break;
            case eStateDead:
            {
                setType(eTileNone);
            }
                break;
            case eStateNone:
            default:
                break;
        }
    }
}

// ******************************************************************************************************************

void AVTile::addBug(AVBug* bug)
{
    mBugs.addObject(bug);
}

// ******************************************************************************************************************

void AVTile::setPosition(const cocos2d::CCPoint& point)
{
//    for (int i = 0; i < mBugs.count(); i++)
//    {
//        ((AVBug*)mBugs.objectAtIndex(i))->moveToPoint(point, 1.0f);
//    }
    CCNodeRGBA::setPosition(point);
}


// ******************************************************************************************************************

void AVTile::setLabel()
{
    char text[10];
    mLabel = CCLabelTTF::create("", "Thonburi", TEXT_SIZE);
    sprintf(text, "%d", mCounter);
    mLabel->setString(text);
    mLabel->setColor(ccWHITE);
    mLabel->setPosition(ccp(TEXT_OFFSET, TEXT_OFFSET));
    this->addChild(mLabel, 1);
}

// ******************************************************************************************************************

void AVTile::updateLabel()
{
    char text[10];
    sprintf(text, "%d", mCounter);
    mLabel->setString(text);
    mLabel->setVisible(true);
}

// ******************************************************************************************************************

void AVTile::hideLabel()
{
    mLabel->setVisible(false);
}

// ******************************************************************************************************************
