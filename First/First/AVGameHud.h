//
//  Hud.h
//  First
//
//  Created by Alexandru Andronache on 9/12/13.
//
//

#ifndef __First__Hud__
#define __First__Hud__

#include <iostream>
#include "AVTile.h"

#define NUM_OBJECTIVES 2

struct Objectives{
    int number;
    AVTile::eTileType type;
};

class AVGameHud : public cocos2d::CCNode
{
public:
    enum EGameEnd{
        EGameEndNone,
        EGameEndVictory,
        EGameEndLost
    };
    AVGameHud();
    ~AVGameHud();
    
    bool init();
    void draw();
    
    void incScore(int score);
    int getScore();
    
    void incMoves(int moves);
    void addObjective(AVTile::eTileType type);
    AVGameHud::EGameEnd getGameEndState();
private:
    void updateLabels();
    
    cocos2d::CCLabelTTF*    mScoreLabel;
    cocos2d::CCLabelTTF*    mMovesLabel;
    cocos2d::CCLabelTTF*    mObjectivesLabel;
    
    Objectives              mObjectives[NUM_OBJECTIVES];
    int                     mMoves;
    int                     mScore;
};

#endif /* defined(__First__Hud__) */
